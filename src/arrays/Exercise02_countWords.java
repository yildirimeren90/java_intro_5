package arrays;

public class Exercise02_countWords {
    public static void main(String[] args) {
        String sentence = "I love arrays";

        int countS = 0;

        for (int i = 0; i < sentence.length(); i++) {
            if(sentence.charAt(i) == ' ') countS++;
        }

        System.out.println(countS + 1);

    }
}
