package arrays;

import java.util.Arrays;

public class _06_doubleArray_Recap {
    public static void main(String[] args) {

        double[] numbers = {1.5, 2.3, -1.3, -3.7};

        System.out.println(Arrays.toString(numbers));
    }
}
