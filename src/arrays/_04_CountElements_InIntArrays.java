package arrays;

public class _04_CountElements_InIntArrays {
    public static void main(String[] args) {


        int[] numbers = {-1, 3, 0, 5, -7, 10, 8, 0, 10, 0};
        int positives = 0;
        for (int number : numbers) {
            if (number > 0) positives++;
        }
        System.out.println("Positive numbers are " + positives);


        int even1 = 0;

        for (int i = 0; i < numbers.length; i++) {
            if(numbers[i] % 2 == 0) even1++;
        }

        System.out.println(even1);

        int even2 = 0;

        for(int number : numbers){
            if(number % 2 == 0) even2++;
        }

        System.out.println(even2);





    }
}
