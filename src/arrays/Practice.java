package arrays;

import java.util.Arrays;

public class Practice {
    public static void main(String[] args) {


        int[] array1 = {5, 8, 13, 1, 2};
        int[] array2 = {9, 3, 67, 1, 0};
        int[] array3 = new int[5];

        for(int i = 0; i < array3.length; i++) {
            array3[i] = Math.max(array1[i], array2[i]);

        }
            System.out.println(Arrays.toString(array1));
            System.out.println(Arrays.toString(array2));
            System.out.println(Arrays.toString(array3));

    }
}
