package practice;

public class ProjectPractice {
    public static void main(String[] args) {
        String s1 = "24", s2 = "5";
        int num1 = Integer.parseInt(s1);
        int num2 = Integer.parseInt(s2);
        System.out.println("The sum of 24 and 5 = " + (num1 + num2));
        System.out.println("The subtraction of 24 and 5 = " + (Math.abs(num1 - num2)));
        System.out.println("The division of 24 and 5 = " + (num1 / num2));
        System.out.println("The multiplication of 24 and 5 = " + (num1 * num2));
        System.out.println("The remainder of 24 and 5 = " + (num1 % num2));


        System.out.println("-----------Task2------------");

        int number = (int) (Math.random() * 35 - 1 + 1) + 1;
        if (number == 1) {
            System.out.println(number + " is not a prime number");
        } else if (number == 2) {
            System.out.println(number + " is a prime number");
        } else if (number % 2 != 0 && number % 3 != 0 && number % 7 != 0) {
            System.out.println(number + " is a prime number");
        } else {
            System.out.println(number + " is not a prime number");
        }

        System.out.println("-----------Task3------------");
        int number1 = (int) (Math.random() * 50 - 1 + 1) + 1;
        int number2 = (int) (Math.random() * 50 - 1 + 1) + 1;
        int number3 = (int) (Math.random() * 50 - 1 + 1) + 1;
        int middle = 0;

        int lowest = (Math.max(Math.max(number1, number2), number3));
        int greatest = (Math.min(Math.min(number1, number2), number3));
        if (number1 == lowest) {
            middle = Math.min(number2, number3);
        } else if (number2 == lowest) {
            middle = Math.min(number1, number3);
        } else if (number3 == lowest) {
            middle = Math.min(number1, number2);
        }
        System.out.println("Lowest number is = " + lowest);
        System.out.println("Middle number is = " + middle);
        System.out.println("Greatest number is = " + greatest);


        int factoriel = 0;
        for (int j = 0; j < 5; j++) {
            factoriel = factoriel * j;
        }
        System.out.println(factoriel);
    }
}
