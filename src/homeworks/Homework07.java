package homeworks;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Collections;

public class Homework07 {
    public static void main(String[] args) {
        System.out.println("\n*******************TASK1*******************\n");

        ArrayList<Integer> numbers1 = new ArrayList<>();
        numbers1.add(10);
        numbers1.add(23);
        numbers1.add(67);
        numbers1.add(23);
        numbers1.add(78);
        System.out.println(numbers1.get(0));
        System.out.println(numbers1.get(1));
        System.out.println(numbers1.get(2));
        System.out.println(numbers1.get(3));
        System.out.println(numbers1.get(4));
        System.out.println(numbers1);

        System.out.println("\n*******************TASK2*******************\n");

        ArrayList<String> colors2 = new ArrayList<>();
        colors2.add("Blue");
        colors2.add("Brown");
        colors2.add("Red");
        colors2.add("White");
        colors2.add("Black");
        colors2.add("Purple");
        System.out.println(colors2.get(1));
        System.out.println(colors2.get(3));
        System.out.println(colors2.get(5));
        System.out.println(colors2);

        System.out.println("\n*******************TASK3*******************\n");

        ArrayList<Integer> numbers3 = new ArrayList<>();
        numbers3.add(23);
        numbers3.add(-34);
        numbers3.add(-56);
        numbers3.add(0);
        numbers3.add(89);
        numbers3.add(100);
        System.out.println(numbers3);
        Collections.sort(numbers3);
        System.out.println(numbers3);

        System.out.println("\n*******************TASK4*******************\n");

        ArrayList<String> cities4 = new ArrayList<>();
        cities4.add("Istanbul");
        cities4.add("Berlin");
        cities4.add("Madrid");
        cities4.add("Paris");

        System.out.println(cities4);
        Collections.sort(cities4);
        System.out.println(cities4);

        System.out.println("\n*******************TASK5*******************\n");

        ArrayList<String> marvel = new ArrayList<>();
        marvel.add("Spider Man");
        marvel.add("Iron Man");
        marvel.add("Black Panter");
        marvel.add("Deadpool");
        marvel.add("Captain America");
        System.out.println(marvel);
        System.out.println(marvel.contains("Wolwerine"));

        System.out.println("\n*******************TASK6*******************\n");

        ArrayList<String> avengers = new ArrayList<>();
        avengers.add("Hulk");
        avengers.add("Black Widow");
        avengers.add("Captain America");
        avengers.add("Iron Man");
        Collections.sort(avengers);
        System.out.println(avengers);
        System.out.println(avengers.contains("Hulk") && avengers.contains("Iron Man"));

        System.out.println("\n*******************TASK7*******************\n");
        ArrayList<Character> characters7 = new ArrayList<>();
        characters7.add('A');
        characters7.add('x');
        characters7.add('$');
        characters7.add('%');
        characters7.add('9');
        characters7.add('*');
        characters7.add('+');
        characters7.add('F');
        characters7.add('G');
        System.out.println(characters7);
        for (Character c : characters7) {
            System.out.println(c);
        }

        System.out.println("\n*******************TASK8*******************\n");

        ArrayList<String> staffs = new ArrayList<>();
        staffs.add("Desk");
        staffs.add("Laptop");
        staffs.add("Mouse");
        staffs.add("Monitor");
        staffs.add("Mouse-Pad");
        staffs.add("Adapter");
        System.out.println(staffs);
        Collections.sort(staffs);
        System.out.println(staffs);

        int count = 0;
        for (String staff : staffs) {
            if (staff.toLowerCase().contains("m")) count++;
        }
        System.out.println(count);

        int countO = 0;
        for (String staff : staffs) {
            if (!staff.toLowerCase().contains("m") && !staff.toLowerCase().contains("e")) countO++;
        }
        System.out.println(countO);

        System.out.println("\n*******************TASK9*******************\n");

        ArrayList<String> kitchen = new ArrayList<>();
        kitchen.add("Plate");
        kitchen.add("spoon");
        kitchen.add("fork");
        kitchen.add("Knife");
        kitchen.add("cup");
        kitchen.add("Mixer");
        System.out.println(kitchen);
        int counter = 0;
        int counter1 = 0;
        int counter2 = 0;
        int counter3 = 0;

        for (String k : kitchen) {
            if (k.charAt(0) >= 65 && k.charAt(0) <= 90) counter++;
            else if (k.charAt(0) >= 97 && k.charAt(0) <= 122) counter1++;
            else if (k.toLowerCase().contains("p")) counter2++;
            else if (k.charAt(0) == 80 || k.charAt(0) == 112
                    || k.charAt(k.length() - 1) == 80 || k.charAt(k.length() - 1) == 112) counter3++;
        }

        System.out.println("Elements starts with uppercase = " + counter);
        System.out.println("Elements starts with lowercase = " + counter1);
        System.out.println("Elements having P or p = " + counter2);
        System.out.println("Elements starting or ending with P or p = " + counter3);

        System.out.println("\n*******************TASK10*******************\n");

        ArrayList<Integer> numbers10 = new ArrayList<>();
        numbers10.add(3);
        numbers10.add(5);
        numbers10.add(7);
        numbers10.add(10);
        numbers10.add(0);
        numbers10.add(20);
        numbers10.add(17);
        numbers10.add(10);
        numbers10.add(23);
        numbers10.add(56);
        numbers10.add(78);
        System.out.println(numbers10);
        int count1 = 0;
        int count2 = 0;
        int count3 = 0;
        int count4 = 0;

        for (Integer number : numbers10) {
            if (number / 10 == 0) count1++;
            else if (number % 2 == 0 && number > 15) count2++;
            else if (number % 2 != 0 && number < 20) count3++;
                else if (number < 15 || number > 50) count4++;
        }
        System.out.println("Elements that can be divided by 10 = " + count1);
        System.out.println("Elements that are even and greater than 15 = " + count2);
        System.out.println("Elements that are odd and less than 20 = " + count3);
        System.out.println("Elements that are less than 15 or greater than 50 = " + count4);
        /* Elements that are odd and less than 20 and
        Elements that are less than 15 or greater than 50
        do not count properly, i couldn't figure out.
        */

    }
}
