package homeworks;

import utilities.ScannerHelper;

import java.util.Scanner;

public class Homework05 {
    public static void main(String[] args) {
        System.out.println("\n====================TASK1====================\n");

        for (int i = 0; i <= 100; i++) {
            if (i % 7 == 0) {
                System.out.print(i + " - ");
            }}
        System.out.println("\n====================TASK2====================\n");

        for (int j = 1; j <= 50; j++) {
            if (j % 6 == 0) {
                System.out.print(j + " - ");
            }}
        System.out.println("\n====================TASK3====================\n");

        for (int k = 100; k >= 50 ; k--) {
            if (k % 5 == 0) {
                System.out.print(k + " - ");
        }}
        System.out.println("\n====================TASK4====================\n");
        for (int g = 0; g <= 7; g++) {
            System.out.print((g * g) + " - ");
        }
            System.out.println("\n====================TASK5====================\n");
        int sum = 0;
        for (int h = 1; h <= 10 ; h++) {
            sum += h;
            System.out.print(sum + " - ");
        }
        System.out.println("\n====================TASK6====================\n");

        int num1 = ScannerHelper.getANumber();
        int factorial = 1;
        for (int k = 1; k <= num1; k++) {
            factorial = factorial * k;
        }System.out.print(factorial);
        System.out.println("\n====================TASK7====================\n");
        String name = ScannerHelper.getAName();
        int counter = 0;

        for (int e = 0; e <= name.length() - 1 ; e++) {
            char ch = name.toLowerCase().charAt(e);
            if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u')
            counter++;
        }
        System.out.println("There are " + counter + " vowel letters in this full name.");

        System.out.println("\n====================TASK8====================\n");

        Scanner input = new Scanner(System.in);
        int sum1 = 0;
        int numb;
        int counter1 = 1;
        do {
            if (counter1 == 1) System.out.println("Please enter a number");
            else System.out.println("Please enter a number");
            numb = input.nextInt();
            sum1 += numb;

            if (numb >= 100) System.out.println("This number is already more than 100");
            else if(sum1 >= 100) System.out.println("Sum of entered numbers is at least 100");
            counter1 ++;
        }
        while(sum1 < 100);

        System.out.println("\n====================TASK9====================\n");

        int num2 = 45, firstTerm = 0, secondTerm = 1;

        for (int i = 1; i <= num2; i++) {
            System.out.print(firstTerm + " - ");
            int nextTerm = firstTerm + secondTerm;
            firstTerm = secondTerm;
            secondTerm = nextTerm;
        }

        System.out.println("\n====================TASK10====================\n");

        String name1;

        do {

            name1 = ScannerHelper.getAName();

        }
        while(name1.toLowerCase().charAt(0) != 'j');

        System.out.println("End of the program");

    }
}




