package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Homework15 {
    public static void main(String[] args) {

        System.out.println("\n---------- TASK 1 ----------\n");

        System.out.println(Arrays.toString(fibonacciSeries1(3)));
        System.out.println(Arrays.toString(fibonacciSeries1(5)));
        System.out.println(Arrays.toString(fibonacciSeries1(7)));

        System.out.println("\n---------- TASK 2 ----------\n");

        System.out.println(fibonacciSeries2(2));
        System.out.println(fibonacciSeries2(4));
        System.out.println(fibonacciSeries2(8));


        System.out.println("\n---------- TASK 3 ----------\n");

        System.out.println(Arrays.toString(findUniques(new int[]{}, new int[]{})));
        System.out.println(Arrays.toString(findUniques(new int[]{}, new int[]{1, 2, 3, 2})));
        System.out.println(Arrays.toString(findUniques(new int[]{1, 2, 3, 4}, new int[]{3, 4, 5, 5})));
        System.out.println(Arrays.toString(findUniques(new int[]{8, 9}, new int[]{9, 8, 9})));

        System.out.println("\n---------- TASK 4 ----------\n");

        System.out.println(isPowerOf3(1));
        System.out.println(isPowerOf3(2));
        System.out.println(isPowerOf3(3));
        System.out.println(isPowerOf3(81));

        System.out.println("\n---------- TASK 5 ----------\n");

        System.out.println(firstDuplicate(new int[]{}));
        System.out.println(firstDuplicate(new int[]{1}));
        System.out.println(firstDuplicate(new int[]{1, 2, 2, 3}));
        System.out.println(firstDuplicate(new int[]{1, 2, 3, 3, 4, 1}));
    }

    public static int[] fibonacciSeries1(int n) {

        int[] arr = new int[n];
        int firstNum = 0, secondNum = 1, thirdNum;

        for (int i = 0; i < n; i++) {
            arr[i] = firstNum;
            thirdNum = firstNum + secondNum;
            firstNum = secondNum;
            secondNum = thirdNum;
        }
        return arr;

    }

    public static int fibonacciSeries2(int n) {

            if (n <= 1) return n;
            int a = 0, b = 1, c = 1;
            for (int i = 2; i < n; i++) {
                c = a + b;
                a = b;
                b = c;
            }
            return c;
        }
    public static Integer[] findUniques(int[] arr1, int[] arr2) {

        ArrayList<Integer> list = new ArrayList<>();

        for (int i = 0; i < arr1.length; i++) {
            boolean isThere = false;
            for (int j = 0; j < arr2.length; j++) {
                if (arr1[i] == arr2[j]) {
                    isThere = true;
                    break;
                }
            }
            if (!isThere && !list.contains(arr1[i])) {
                list.add(arr1[i]);
            }
        }
        for (int i = 0; i < arr2.length; i++) {
            boolean isThere = false;

            for (int j = 0; j < arr1.length; j++) {
                if (arr2[i] == arr1[j]) {
                    isThere = true;
                    break;
                }
            }
            if(!isThere && !list.contains(arr2[i])) {
                list.add(arr2[i]);
            }
        }
        return list.toArray(new Integer[0]);
    }

    public static boolean isPowerOf3(int n){

        if (n == 1) return true;
        while (n > 0) {
            if (n % 3 != 0) return false;
            if (n == 3) return true;
            n /= 3;
        }
        return false;
    }

    public static int firstDuplicate(int[] arr) {

        Set<Integer> set = new HashSet<>();
        for(int num : arr){
            if(!set.add(num)){
                return num;
            }
        }
        return -1;
    }


}

