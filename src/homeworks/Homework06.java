package homeworks;

        import java.util.Arrays;

public class Homework06 {
    public static void main(String[] args) {

        System.out.println("\n==================TASK1==================\n");

        int[] numbers1 = {89, 0, 23, 0, 12, 0, 15, 34, 0, 7};
        System.out.println(numbers1[3]);
        System.out.println(numbers1[0]);
        System.out.println(numbers1[9]);
        System.out.println(Arrays.toString(numbers1));
        System.out.println("\n==================TASK2==================\n");
        String[] elements2 = new String[5];
        elements2[1] = "abc";
        elements2[4] = "xyz";
        System.out.println(elements2[3]);
        System.out.println(elements2[0]);
        System.out.println(elements2[4]);
        System.out.println(Arrays.toString(elements2));


        System.out.println("\n==================TASK3==================\n");

        int[] numbers3 = {23, -34, -56, 0, 89, 100};

        System.out.println(Arrays.toString(numbers3));
        Arrays.sort(numbers3);
        System.out.println(Arrays.toString(numbers3));

        System.out.println("\n==================TASK4==================\n");

        String[] countries4 = {"Germany", "Argentina", "Ukraine", "Romania"};

        System.out.println(Arrays.toString(countries4));
        Arrays.sort(countries4);
        System.out.println(Arrays.toString(countries4));


        System.out.println("\n==================TASK5==================\n");

        String[] cartoon5 = {"Scooby Doo", "Snoopy", "Blue", "Pluto", "Dino", "Sparky"};

        System.out.println(Arrays.toString(cartoon5));
        boolean hasPluto = false;
        for(String dogs : cartoon5){
            if(dogs.equals("Pluto")){
                hasPluto = true;
                break;
            }
        }
        System.out.println(hasPluto);



        System.out.println("\n==================TASK6==================\n");

        String[] cartoon6 = {"Garfield", "Tom", "Sylvester", "Azrael"};
        Arrays.sort(cartoon6);
        System.out.println(Arrays.toString(cartoon6));
        boolean hasGarFel = false;
        for(String cats: cartoon5){
            if(cats.equals("Pluto") && cats.equals("Felix")){
                hasGarFel = true;
                break;
            }
        }
        System.out.println(hasGarFel);

        System.out.println("\n==================TASK7==================\n");

        double[] numbers7 = {10.5, 20.75, 70, 80, 15.75};

        System.out.println(Arrays.toString(numbers7));

        for(double number7 : numbers7) {
            System.out.println(number7);
        }

        System.out.println("\n==================TASK8==================\n");



        char[] arr = {'A', 'b', 'G', 'H', '7', '5', '&', '*', 'e', '@', '4'};
        System.out.println(Arrays.toString(arr));
        int letters = 0;
        int lowers = 0;
        int uppers = 0;
        int digits = 0;
        int specials =0;

        for(char a : arr){
            if(Character.isLowerCase(a)) lowers++;
            else if(Character.isUpperCase(a)) uppers++;
            else if(Character.isDigit(a)) digits++;
            else if (a != ' ') specials++;
        }
        System.out.println("Letters = " + (lowers + uppers));
        System.out.println("Lower case Letters = " + lowers);
        System.out.println("Upper case Letters = " + uppers);
        System.out.println("Digits = " + digits);
        System.out.println("Special characters = " + specials);


        System.out.println("==================TASK9==================");
        String[] objects9 = {"Pen", "notebook", "Book", "paper", "bag", "pencil", "Ruler"};
        System.out.println(Arrays.toString(objects9));
        int uCase = 0;
        int lCase = 0;
        int startsWithBP = 0;
        int book = 0;
        int bookOrPe = 0;

        System.out.println("==================TASK10==================");

        int moreThan10 = 0;
        int lessThan10 = 0;
        int is10 = 0;

        int[] numbers10 = {3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78};
        System.out.println(Arrays.toString(numbers10));

        for (int number10 : numbers10) {
            if (number10 > 10) moreThan10++;
            else if (number10 < 10) lessThan10++;
            else if (number10 == 10) is10++;
        }
        System.out.println("Elements that are more than 10 = " + moreThan10);
        System.out.println("Elements that are less than 10 = " + lessThan10);
        System.out.println("Elements that are 10 = " + is10);

        System.out.println("==================TASK11==================");

        int[] nFirst = {5, 8, 13, 1, 2};
        int[] nSecond = {9, 3, 67, 1, 0};
        System.out.println(Arrays.toString(nFirst));
        System.out.println(Arrays.toString(nSecond));

        int max1 = Math.max(nFirst[0], nSecond[0]);
        int max2 = Math.max(nFirst[1], nSecond[1]);
        int max3 = Math.max(nFirst[2], nSecond[2]);
        int max4 = Math.max(nFirst[3], nSecond[3]);
        int max5 = Math.max(nFirst[4], nSecond[4]);

        int[] nThird = {max1, max2, max3, max4, max5};
        System.out.println("1st array is = " + Arrays.toString(nFirst));
        System.out.println("2nd array is = " + Arrays.toString(nSecond));
        System.out.println("3rd array is = " + Arrays.toString(nThird));
    }
}
