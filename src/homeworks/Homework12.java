package homeworks;

import java.util.Arrays;

public class Homework12 {

    public static void main(String[] args) {

        System.out.println("\n-----------TASK 1-----------\n");

        System.out.println(noDigit(""));
        System.out.println(noDigit("java"));
        System.out.println(noDigit("123Hello"));
        System.out.println(noDigit("123Hello World149"));
        System.out.println(noDigit("123Tech456Global149"));

        System.out.println("\n-----------TASK 2-----------\n");

        System.out.println(noVowel(""));
        System.out.println(noVowel("xyz"));
        System.out.println(noVowel("JAVA"));
        System.out.println(noVowel("125$"));
        System.out.println(noVowel("TechGlobal"));

        System.out.println("\n-----------TASK 3-----------\n");

        System.out.println(sumOfDigits(""));
        System.out.println(sumOfDigits("Java"));
        System.out.println(sumOfDigits("John's age is 29"));
        System.out.println(sumOfDigits("$125.0"));

        System.out.println("\n-----------TASK 4-----------\n");

        System.out.println(hasUpperCase(""));
        System.out.println(hasUpperCase("java"));
        System.out.println(hasUpperCase("John's age is 29"));
        System.out.println(hasUpperCase("$125.0"));

        System.out.println("\n-----------TASK 5-----------\n");

        System.out.println(middleInt(1, 1, 1));
        System.out.println(middleInt(1, 2, 2));
        System.out.println(middleInt(5, 5, 8));
        System.out.println(middleInt(5, 3, 5));
        System.out.println(middleInt(-1, 25, 10));

        System.out.println("\n-----------TASK 6-----------\n");

        System.out.println(Arrays.toString(no13(new int[]{1, 2, 3, 4})));
        System.out.println(Arrays.toString(no13(new int[]{13, 2, 3})));
        System.out.println(Arrays.toString(no13(new int[]{13, 13, 13, 13, 13})));

        System.out.println("\n-----------TASK 7-----------\n");

        int[] arr1 = {1, 2, 3, 4};
        System.out.println(Arrays.toString(arrFactorial(arr1)));

        int[] arr2 = {0, 5};
        System.out.println(Arrays.toString(arrFactorial(arr2)));

        int[] arr3 = {5, 0, 6};
        System.out.println(Arrays.toString(arrFactorial(arr3)));

        System.out.println("\n-----------TASK 8-----------\n");

        System.out.println(Arrays.toString(categorizeCharacters("     ")));
        System.out.println(Arrays.toString(categorizeCharacters("abc123$#%")));
        System.out.println(Arrays.toString(categorizeCharacters("12ab$%3c%")));

    }

    public static String noDigit(String str) {

        return str.replaceAll("[\\d]", "");
    }

    public static String noVowel(String str) {

        return str.replaceAll("[AEIOUaeiou]", "");
    }

    public static int sumOfDigits(String str) {

        int sumOfDigits = 0;
        for (int i = 0; i < str.length(); i++) {
            if (Character.isDigit(str.charAt(i))) sumOfDigits += Character.getNumericValue(str.charAt(i));

        }
        return sumOfDigits;
    }

    public static boolean hasUpperCase(String str) {

        boolean hasUCase = false;
        for (int i = 0; i < str.length(); i++) {
            if (Character.isUpperCase(str.charAt(i)))
                hasUCase = true;
        }

        return hasUCase;
    }

    public static int middleInt(int i1, int i2, int i3) {

        int max = Math.max(Math.max(i1, i2), i3);
        int min = Math.min(Math.min(i1, i2), i3);
        int middle = (i1 + i2 + i3) - min - max;

        return middle;
    }

    public static int[] no13(int[] array) {

        int[] newArray = new int[array.length];

        for (int i = 0; i < array.length; i++) {
            if (array[i] == 13) {
                newArray[i] = 0;
            } else {
                newArray[i] = array[i];
            }
        }
        return newArray;
    }

    public static int[] arrFactorial(int[] numbers) {

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] == 0) numbers[i] = 1;
            else {
                int factorial = 1;
                for (int j = 1; j <= numbers[i]; j++) {
                    factorial *= j;
                }
                numbers[i] = factorial;
            }
        }
        return numbers;
    }

    public static String[] categorizeCharacters(String str) {

        String[] newArray = {"","",""};
        for (int i = 0; i < str.length(); i++) {
            if(Character.isLetter(str.charAt(i))) newArray[0] += str.charAt(i);
            else if(Character.isDigit(str.charAt(i))) newArray[1] += str.charAt(i);
            else newArray[2] += str.charAt(i);
        }
        return newArray;
    }
}

