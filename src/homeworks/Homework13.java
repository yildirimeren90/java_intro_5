package homeworks;

import java.util.*;

public class Homework13 {
    public static void main(String[] args) {

        System.out.println("\n------------TASK 1------------\n");

        System.out.println(hasLowerCase(""));
        System.out.println(hasLowerCase("JAVA"));
        System.out.println(hasLowerCase("125$"));
        System.out.println(hasLowerCase("hello"));

        System.out.println("\n------------TASK 2------------\n");

        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1));
        System.out.println(noZero(list));
        ArrayList<Integer> list2 = new ArrayList<>(Arrays.asList(1, 1, 10));
        System.out.println(noZero(list2));
        ArrayList<Integer> list3 = new ArrayList<>(Arrays.asList(0, 1, 10));
        System.out.println(noZero(list3));
        ArrayList<Integer> list4 = new ArrayList<>(Arrays.asList(0, 0, 0));
        System.out.println(noZero(list4));

        System.out.println("\n------------TASK 3------------\n");

        System.out.println(Arrays.deepToString(numberAndSquare(new int[]{1,2,3})));
        System.out.println(Arrays.deepToString(numberAndSquare(new int[]{0,3,6})));
        System.out.println(Arrays.deepToString(numberAndSquare(new int[]{1,4})));


        System.out.println("\n------------TASK 4------------\n");

        System.out.println(containsValue(new String[]{"abc", "foo", "java"}, "hello"));
        System.out.println(containsValue(new String[]{"abc", "def", "123"}, "Abc"));
        System.out.println(containsValue(new String[]{"abc", "def", "123", "Java", "Hello"}, "123"));


        System.out.println("\n------------TASK 5------------\n");

        System.out.println(reverseSentence("Java is Fun"));
        System.out.println(reverseSentence("This is a sentence"));


        System.out.println("\n------------TASK 6------------\n");

        System.out.println(removeStringSpecialsDigits("123Java #$%is fun"));
        System.out.println(removeStringSpecialsDigits("Selenium"));
        System.out.println(removeStringSpecialsDigits("Selenium 123#$%Cypress"));

        System.out.println("\n------------TASK 7------------\n");

        System.out.println(Arrays.toString(removeArraySpecialsDigits(new String[]{"123Java", "#$%is", "fun"})));
        System.out.println(Arrays.toString(removeArraySpecialsDigits(new String[]{"Selenium", "123$%", "###"})));
        System.out.println(Arrays.toString(removeArraySpecialsDigits(new String[]{"Selenium", "123$%Cypress"})));

        System.out.println("\n------------TASK 8------------\n");

        System.out.println(removeAndReturnCommons(new ArrayList<>(Arrays.asList("Java", "is", "fun")),
                new ArrayList<>(Arrays.asList("abc", "xyz", "123"))));

        System.out.println(removeAndReturnCommons(new ArrayList<>(Arrays.asList("Java", "is", "fun")),
                new ArrayList<>(Arrays.asList("Java", "C#", "Python"))));

        System.out.println(removeAndReturnCommons(new ArrayList<>(Arrays.asList("Java", "C#", "C#")),
                new ArrayList<>(Arrays.asList("Python", "C#", "C++"))));

        System.out.println("\n------------TASK 9------------\n");

        ArrayList<String> empty = new ArrayList<>(Arrays.asList("abc","123","#$%"));
        System.out.println(noXInVariables(empty));

        ArrayList<String> empty1 = new ArrayList<>(Arrays.asList("xyz","123","#$%"));
        System.out.println(noXInVariables(empty1));

        ArrayList<String> empty2 = new ArrayList<>(Arrays.asList("x","123","#$%"));
        System.out.println(noXInVariables(empty2));

        ArrayList<String> empty3 = new ArrayList<>(Arrays.asList("xyXyxy","Xx","ABC"));
        System.out.println(noXInVariables(empty3));

    }

    public static boolean hasLowerCase(String str) {
        str = str.trim();
        boolean isFound = false;
        for (int i = 0; i < str.length(); i++) {

            if (Character.isLowerCase(str.charAt(i))) {
                isFound = true;
                break;
            }
        }
        return isFound;

    }

    public static ArrayList<Integer> noZero(ArrayList<Integer> list) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) == 0) list.removeAll(Collections.singleton(list.get(i)));
        }
        return list;
    }


    public static int[][] numberAndSquare(int[] arr) {
        int[][] mDim = new int[arr.length][2];

        for (int i = 0; i < arr.length; i++) {
            mDim[i][0] = arr[i];
            mDim[i][1] = arr[i] * arr[i];
        } return mDim;
    }

    public static boolean containsValue(String[] arr, String str) {

        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equalsIgnoreCase(str)) return true;
        }
        return false;
    }

    public static String reverseSentence(String str) {
        String firstL = str.substring(str.lastIndexOf(" ") + 1);
        String restL = str.substring(str.indexOf(" "), str.lastIndexOf(" ") + 1).toLowerCase();
        String lastL = str.toLowerCase().substring(0, str.indexOf(" "));

        return (firstL.toUpperCase().charAt(0) + firstL.substring(1) + restL + lastL);
    }

    public static String removeStringSpecialsDigits(String str) {

        str = str.trim().replaceAll("[^\\w ]", "");
        return str.replaceAll("[\\d]", "");
    }

    public static String[] removeArraySpecialsDigits(String[] arr) {

        for (int i = 0; i < arr.length; i++) {
            arr[i] = arr[i].replaceAll("[\\d$%#@!&^*+]", "");
        }
        return arr;
    }

    public static ArrayList<String> removeAndReturnCommons(ArrayList<String> str, ArrayList<String> str2) {

        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < str.size(); i++) {
            for (int j = 0; j < str2.size(); j++) {
                if (str.get(i).equals(str2.get(j)) && !list.contains(str.get(i)))
                    list.add(str.get(i));
            }
        } return list;
    }

    public static List<String> noXInVariables(List<String> list) {

        ListIterator<String> iterator = list.listIterator();

        while (iterator.hasNext()) {
            String s = iterator.next();
            if (s.toLowerCase().contains("x")) {
                iterator.set(s.toLowerCase().replaceAll("x", ""));
            }
        }
        return list;
    }

}

