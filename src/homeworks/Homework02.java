package homeworks;

import com.sun.xml.internal.ws.developer.MemberSubmissionAddressing;

import javax.xml.validation.Validator;
import java.sql.SQLOutput;
import java.util.Scanner;

public class Homework02 {
    public static void main(String[] args) {

        System.out.println("\n=====================TASK1=====================\n");

        Scanner inputReader = new Scanner(System.in);


        int num1 = 5, num2 = 10;
        System.out.println("Hey user, can you please enter number 1!");
        num1= inputReader.nextInt();
        System.out.println("The number 1 entered by user = " + num1 + "\n");
        System.out.println("Can you please now, enter number 2!");
        num2 = inputReader.nextInt();
        System.out.println("The number 2 entered by user is = " + num2 + "\n");
        System.out.println("The sum of number 1 and 2 entered by user is = " + (num1+num2));




        System.out.println("\n=====================TASK2==========================\n");
        int number = 3, number2 = 5;
        System.out.println("Hey user, tell me your first number!");
        number= inputReader.nextInt();
        System.out.println("Now, tell me your second number!");
        number2 = inputReader.nextInt();
        System.out.println("The product of the given 2 numbers is = " + (number * number2) + "\n");



        System.out.println("\n====================TASK3============================\n");
        double number01 = 24, number02 = 10;
        System.out.println("Please enter number01");
        number01 = inputReader.nextDouble();
        System.out.println("Please enter number02");
        number02 = inputReader.nextDouble();
        double sum1 = number01 + number02;
        System.out.println("The sum of the given number is = " + sum1 +"\n");
        double multiplication1 = number01 * number02;
        System.out.println("The product of the given number is = " + multiplication1 +"\n");
        double subtraction = number01 - number02;
        System.out.println("The subtraction of the given numbers is = " + subtraction +"\n");
        double division = number01 / number02;
        System.out.println("The division of the given numbers is = " + division +"\n");
        double remainder = number01 % number02;
        System.out.println("The remainder of the given numbers is = " + remainder);

        System.out.println("\n========================TASK4============================\n");
        System.out.println( -10 + 7 * 5);
        System.out.println((72 + 24)  % 24);
        System.out.println( 10 + - 3*9 / 9);
        System.out.println(5 + 18/3 * 3 - (6 % 3));

        System.out.println("\n========================TASK5===============================\n");
        int numb1 = 7, numb2 = 11;
        System.out.println("Hey user, please enter first number! ");
        numb1 = inputReader.nextInt();
        System.out.println("Now, please enter second number! ");
        numb2 = inputReader.nextInt();
        sum1 = numb1 + numb2;
        System.out.println("The average of the given numbers is = " + (sum1) / 2);


        System.out.println("\n=============================TASK6=========================\n");
        int firstN=6 , secondN=10, thirdN=12, fourN=15, fifthN=17;
        System.out.println("Hey user, what is your first number?");
        firstN = inputReader.nextInt();
        System.out.println("What is your second number?");
        secondN = inputReader.nextInt();
        System.out.println("What is your third number?");
        thirdN = inputReader.nextInt();
        System.out.println("What is your four number?");
        fourN = inputReader.nextInt();
        System.out.println("What is your fifth number?");
        fifthN = inputReader.nextInt();
        System.out.println("The average of the given numbers is = " + (firstN+secondN+thirdN+fourN+fifthN) / 5);




        System.out.println("\n==========================TASK7=============================\n");
        int firstNumber = 5, secondNumber= 6, thirdNumber = 10;
        System.out.println("Hey user, please tell me your first number!");
        firstNumber = inputReader.nextInt();
        System.out.println("The 5 multiplied with 5 is = " + firstNumber*firstNumber +"\n");
        System.out.println("Hey user, please tell me your second number!");
        secondNumber = inputReader.nextInt();
        System.out.println("The 6 multiplied with 6 is = " + secondNumber * secondNumber + "\n");
        System.out.println("Hey user, tell me your third number!");
        thirdNumber = inputReader.nextInt();
        System.out.println("The 10 multiplied with 10 is = " + thirdNumber * thirdNumber + "\n");


        System.out.println("\n========================TASK8==============================\n");
        int side = 7;
        System.out.println("Hey user, Tell me what is one side of a square equal to? ");
        side = inputReader.nextInt();
        System.out.println("The Area of the square = " + side*side + "\n");
        System.out.println("The Perimeter of the square = " + (4 * side));

        System.out.println("\n=======================TASK9==================================\n");
        double annualSalary = 90000;
        System.out.println("A Software Engineer in Test can earn "+  "$" + (annualSalary*3) + " in 3 years.");

        System.out.println("\n=========================TASK10==================================\n");
        String favBook = "HarryPotter";
        String favColor = "Black";
        int  favNumber = 6;


        System.out.println("\nHey user, What is your favorite book?");
        favBook =inputReader.nextLine();



        System.out.println("\nWhat is your favorite color?");
        favColor = inputReader.next();
        inputReader.nextLine();


        System.out.println("\nWhat's your favorite number? ");
        favNumber = inputReader.nextInt();

        System.out.println("User's favorite book is: " + favBook +
                "\nUser's favorite color is: " + favColor +
                "\nUser's favorite number is: " + favNumber);


        System.out.println("\n=========================TASK11===================================\n");

        String firstName = "John";
        String lastName = "Doe";
        int    age = 45;
        String emailAddress = "johndoe@gmail.com";




        String phoneNumber = "(123) 123 1234";
        String address = "St Chicago IL 12345";

        System.out.println("Hey, User what is your first name?\n");
        firstName = inputReader.nextLine();

        System.out.println("Hey user, what is your last name?\n");
        lastName = inputReader.nextLine();

        System.out.println("Hey user, what is your age?\n");
        age = inputReader.nextInt();
        inputReader.nextLine();



        System.out.println("Hey user, what is your email address?\n");
        emailAddress = inputReader.nextLine();

        System.out.println("Hey user, what is your phone number?\n");
        phoneNumber = inputReader.nextLine();

        System.out.println("Now, please tell me your address.\n");
        address = inputReader.nextLine();

        System.out.println("\t\tUser who joined this program is " + firstName +" " + lastName + "." + firstName+"'s age is " +
                "\n" + age +"." + firstName+"'s email address is " + emailAddress + "," + "phone number" +
                "\nis " + phoneNumber +"," + " and address is " + address + ".");


    }
}
