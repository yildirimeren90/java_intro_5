package homeworks;

import java.util.Arrays;

public class Homework14 {

    public static void main(String[] args) {


        System.out.println("\n---------- TASK 1 ----------\n");

        fizzBuzz1(3);
        fizzBuzz1(5);
        fizzBuzz1(18);

        System.out.println("\n---------- TASK 2 ----------\n");

        System.out.println(fizzBuzz2(0));
        System.out.println(fizzBuzz2(1));
        System.out.println(fizzBuzz2(3));
        System.out.println(fizzBuzz2(5));
        System.out.println(fizzBuzz2(15));

        System.out.println("\n---------- TASK 3 ----------\n");

        System.out.println(findSumNumbers("abc$"));
        System.out.println(findSumNumbers("a1b4c 6#"));
        System.out.println(findSumNumbers("ab110c045d"));
        System.out.println(findSumNumbers("525"));

        System.out.println("\n---------- TASK 4 ----------\n");

        System.out.println(findBiggestNumber("abc$"));
        System.out.println(findBiggestNumber("a1b4c 6#"));
        System.out.println(findBiggestNumber("ab110c045d"));
        System.out.println(findBiggestNumber("525"));


        System.out.println("\n---------- TASK 5 ----------\n");
        System.out.println(countSequenceOfCharacters(""));
        System.out.println(countSequenceOfCharacters("abc"));
        System.out.println(countSequenceOfCharacters("abbcca"));
        System.out.println(countSequenceOfCharacters("aaAAa"));

    }

    public static void fizzBuzz1(int n) {

        for (int i = 1; i <= n; i++) {

            if (i % 15 == 0) System.out.println("FizzBuzz");
            else if (i % 3 == 0) System.out.println("Fizz");
            else if (i % 5 == 0) System.out.println("Buzz");
            else System.out.println(i);
        }
    }

    public static String fizzBuzz2(int n) {

        if (n % 15 == 0) return "FizzBuzz";
        else if (n % 3 == 0) return ("Fizz");
        else if (n % 5 == 0) return ("Buzz");
        else return String.valueOf(n);

    }

    public static int findSumNumbers(String str) {

        int sum = 0;
        String sum1 = "0";

        for (int i = 0; i < str.length(); i++) {

            if (Character.isDigit(str.charAt(i))) {
                sum1 += str.charAt(i);
            } else {
                sum += Integer.parseInt(sum1);
                sum1 = "0";
            }
        }
        return sum + Integer.parseInt(sum1);
    }

    public static int findBiggestNumber(String str) {

        try {
            String[] arr = str.split("[^0-9]");
            Arrays.sort(arr);
            int biggest = Integer.parseInt(arr[arr.length - 1]);
            return biggest;
        } catch (Exception e) {
            return 0;
        }
    }

    public static String countSequenceOfCharacters(String str){
        int count = 1;
        String result = "";
        for (int i = 0; i < str.length() - 1; i++) {
            if(str.charAt(i) == str.charAt(i+1)) count++;
            else {
                result += String.valueOf(count) + str.charAt(i);
                count = 1;
            }
            if(i == str.length() - 2) result += String.valueOf(count) + str.charAt(i + 1);
        }
        return result;
    }

}
