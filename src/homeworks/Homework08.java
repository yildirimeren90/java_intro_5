package homeworks;

import utilities.ScannerHelper;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Homework08 {

    public static int countConsonants(String str) {

        str = str.replaceAll("[a-zA-z]","");
        str = str.replaceAll("[aeiouAEIOU]","");
        return str.length();
    }

    public static String wordArray(String str) {

        String[] arr = str.split("[^A-Za-z]");
        System.out.println(Arrays.toString(arr));
        return str;
    }

    public static String removeExtraSpaces(String str) {

        System.out.println(str.replaceAll("[\\s]{2,}"," "));
        return str;
    }

    public static int count3OrLess(String str) {

        str = ScannerHelper.getAString();
        String[] arr = str.split("[a-zA-Z]{4,}");
        Pattern pattern = Pattern.compile("[a-zA-Z]{1,3}");
        Matcher matcher = pattern.matcher(Arrays.toString(arr));

        int count = 0;
        while (matcher.find()) {
            count++;
        }
        return count;
    }

    public static boolean isDateFormatValid(String dateOfBirth) {

        return dateOfBirth.matches("[\\d]{2}(/|)[\\d]{2}(/|)[\\d]{4}");
    }

    public static boolean isEmailFormatValid(String email) {

        return Pattern.matches("[\\w]{2,}@[\\w]{2,}\\.[\\w]{2,}",email);
    }

    public static void main(String[] args) {


        System.out.println("\n**********************TASK1**********************\n");

    String str1 = "hello";
    String st1 = "JAVA";
    String s1 = "";

        System.out.println(countConsonants(str1));
        System.out.println(countConsonants(st1));
        System.out.println(countConsonants(s1));

        System.out.println("\n**********************TASK2**********************\n");
        String str2 = "hello";
        String st2 = "java is  fun";
        String s2 = "Hello,nice to meet you!!";
        wordArray(str2);
        wordArray(st2);
        wordArray(s2);

        System.out.println("\n**********************TASK3**********************\n");

    String str3 = "hello";
    String st3 = "java is  fun";
    String s3 = "Hello,    nice to   meet     you!!";
    removeExtraSpaces(str3);
    removeExtraSpaces(st3);
    removeExtraSpaces(s3);

        System.out.println("\n**********************TASK4**********************\n");

    String str4 = "I go to TechGlobal";
    String st4 = "Hi, my name is John Doe";
    String s4 = "Hello guys";

        System.out.println(count3OrLess(str4));
        System.out.println(count3OrLess(st4));
        System.out.println(count3OrLess(s4));

        System.out.println("\n**********************TASK5**********************\n");

        System.out.println(isDateFormatValid("01/21/1999"));
        System.out.println(isDateFormatValid("1/20/1991"));
        System.out.println(isDateFormatValid("10/2/1991"));
        System.out.println(isDateFormatValid("12-20 2000"));
        System.out.println(isDateFormatValid("12/16/19500"));

        System.out.println("\n**********************TASK6**********************\n");

    String email1 = "abc@gmail.com";
    String email2 = "abc@student.techglobal.com";
    String email3 = "a@gmail.com";
    String email4 = "abcd@@gmail.com";
    String email5 = "abc@gmail";

        System.out.println(isEmailFormatValid(email1));
        System.out.println(isEmailFormatValid(email2));
        System.out.println(isEmailFormatValid(email3));
        System.out.println(isEmailFormatValid(email4));
        System.out.println(isEmailFormatValid(email5));


    }

}







