package homeworks;

import java.util.Arrays;

public class Homework11 {
    public static void main(String[] args) {

        System.out.println("\n----------- TASK 1 -----------\n");


        System.out.println(noSpace(""));
        System.out.println(noSpace("Java"));
        System.out.println(noSpace("  Hello "));
        System.out.println(noSpace(" Hello World "));
        System.out.println(noSpace("Tech Global"));

        System.out.println("\n----------- TASK 2 -----------\n");

        System.out.println(replaceFirstLast(""));
        System.out.println(replaceFirstLast("A"));
        System.out.println(replaceFirstLast("   A    "));
        System.out.println("Hello");

        System.out.println("\n----------- TASK 3 -----------\n");

        System.out.println(hasVowel(""));
        System.out.println(hasVowel("Java"));
        System.out.println(hasVowel("1234"));
        System.out.println(hasVowel("ABC"));

        System.out.println("\n----------- TASK 4 -----------\n");

        System.out.println(checkAge(2010));
        System.out.println(checkAge(2006));
        System.out.println(checkAge(2050));
        System.out.println(checkAge(1920));
        System.out.println(checkAge(1800));

        System.out.println("\n----------- TASK 5 -----------\n");

        System.out.println(averageOfEdges(0, 0, 0));
        System.out.println(averageOfEdges(0, 0, 6));
        System.out.println(averageOfEdges(-2, -2, 10));
        System.out.println(averageOfEdges(-3, 15, -3));
        System.out.println(averageOfEdges(10, 13, 20));

        System.out.println("\n----------- TASK 6 -----------\n");

        String[] arr1 = {"java", "hello", "123", "xyz"};
        System.out.println(Arrays.toString(noA(arr1)));

        String[] arr2 = {"appium", "123", "ABC", "java"};
        System.out.println(Arrays.toString(noA(arr2)));

        String[] arr3 = {"apple", "appium", "ABC", "Alex", "A"};
        System.out.println(Arrays.toString(noA(arr3)));

        System.out.println("\n----------- TASK 7 -----------\n");

        int[] arr4 = {7, 4, 11, 23, 17};
        System.out.println(Arrays.toString(no3or5(arr4)));

        int[] arr5 = {3, 4, 5, 6};
        System.out.println(Arrays.toString(no3or5(arr5)));

        int[] arr6 = {10, 11, 12, 13, 14, 15};
        System.out.println(Arrays.toString(no3or5(arr6)));

        System.out.println("\n----------- TASK 8 -----------\n");

        int[] arr7 = {-10,-3,0,1};
        System.out.println(countPrimes(arr7));

        int[] arr8 = {7,4,11,23,17};
        System.out.println(countPrimes(arr8));

        int[] arr9 = {41,53,19,47,67};
        System.out.println(countPrimes(arr9));

    }

    public static String noSpace(String str) {

        return str.trim().replaceAll("[\\s]", "");
    }

    public static String replaceFirstLast(String str) {

        str = str.trim();
        if (str.length() < 2)
            return str;

        StringBuilder sb = new StringBuilder(str);
        char firstLetter = sb.charAt(0);

        sb.setCharAt(0, sb.charAt(sb.length() - 1));
        sb.setCharAt(sb.length() - 1, firstLetter);

        return sb.toString();
    }

    public static boolean hasVowel(String str) {

        for (int i = 0; i < str.length(); i++) {
            String s = str.toLowerCase();
            if (s.charAt(i) == 'a' || s.charAt(i) == 'e' || s.charAt(i) == 'o' || s.charAt(i) == 'u' ||
                    s.charAt(i) == 'i') return true;
        }
        return false;
    }
    public static String checkAge(int year) {
        int currentYear = 2022;
        int calculation = currentYear - year;
        if (calculation > 100 || year > currentYear) return ("AGE IS NOT VALID");
        else if (calculation < 16) return ("AGE IS NOT ALLOWED");
        else return ("AGE IS ALLOWED");
    }
    public static int averageOfEdges(int num1, int num2, int num3) {

        int max = Math.max(Math.max(num1, num2), num3);
        int min = Math.min(Math.min(num1, num2), num3);
        return (max + min) / 2;
    }public static String[] noA(String[] array) {

        String[] result = new String[array.length];
        for (int i = 0; i < array.length; i++) {
            if (array[i].toLowerCase().startsWith("a")) result[i] = "###";

            else result[i] = array[i];
        }
        return result;
    }

    public static int[] no3or5(int[] array) {

        int[] numbers = new int[array.length];
        if (array.length > 0) {
            for (int i = 0; i < array.length; i++) {
                if (array[i] % 5 == 0 && array[i] % 3 == 0) numbers[i] = 101;
                else if (array[i] % 5 == 0) numbers[i] = 99;
                else if (array[i] % 3 == 0) numbers[i] = 100;
                else numbers[i] = array[i];
            }
        }
        return numbers;
    }
    public static int countPrimes(int[] numbers){
        int count = 0;
        for (int number : numbers) {
            if (number == 2 || number == 3) count++;
            else if (number > 3) {
                boolean isPrime = true;
                for (int i = 2; i < number; i++) {
                    if(number % i == 0) {
                        isPrime = false;
                    }
                }
                if (isPrime) count++;
            }
        }
        return count;
    }
}


