package homeworks;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Homework16 {

    public static void main(String[] args) {

        System.out.println("\n---------- TASK 1 ----------\n");

        System.out.println(parseData("{104}LA{101}Paris{102}Berlin{103}Chicago{100}London"));

        System.out.println("\n---------- TASK 2 ----------\n");

        HashMap<String, Integer> map1 = new HashMap<>();
        map1.put("Apple", 3);
        map1.put("Mango", 1);

        System.out.println(calculateTotalPrice1(map1));

        HashMap<String, Integer> map2 = new HashMap<>();

        map2.put("Apple", 2);
        map2.put("Pineapple", 1);
        map2.put("Orange", 3);

        System.out.println(calculateTotalPrice1(map2));


        System.out.println("\n---------- TASK 3 ----------\n");

        Map<String, Integer> fruits1 = new HashMap<>();
        fruits1.put("Apple", 3);
        fruits1.put("Mango", 5);
        System.out.println(calculateTotalPrice2(fruits1));

        Map<String, Integer> fruits2 = new HashMap<>();
        fruits2.put("Apple", 4);
        fruits2.put("Mango", 8);
        fruits2.put("Orange", 3);
        System.out.println(calculateTotalPrice2(fruits2));


    }

    public static Map<Integer, String> parseData(String str) {

        String[] str2 = str.replaceAll("[{}]", " ").trim().split(" ");

        Map<Integer, String> map = new TreeMap<>();

        for (int i = 0; i < str2.length; i++) {
            map.put(Integer.parseInt(str2[i]), str2[i + 1]);
            i++;
        }
        return map;
    }

    public static double calculateTotalPrice1(Map<String, Integer> map) {

        HashMap<String, Double> newMap = new HashMap<>();

        newMap.put("Apple", 2.00);
        newMap.put("Orange", 3.29);
        newMap.put("Mango", 4.99);
        newMap.put("Pineapple", 5.25);

        double totalPrice = 0.0;

        for (String keys : map.keySet()) {
            totalPrice += map.get(keys) * newMap.get(keys);
        }
        return totalPrice;
    }

    public static double calculateTotalPrice2(Map<String, Integer> map) {

        HashMap<String, Double> fruitMap = new HashMap<>();

        fruitMap.put("Apple", 2.00);
        fruitMap.put("Orange", 3.29);
        fruitMap.put("Mango" , 4.99);
        fruitMap.put("Pineapple", 5.25);

        double totalPrice = 0.0;

        if (map.get("Mango") >= 3) {
            map.put("Mango", map.get("Mango") - (map.get("Mango") / 3));

        }
        for (String element : map.keySet()) {

            if (element.equals("Apple")) {
                if (map.get("Apple") % 2 == 1) {
                    totalPrice += (map.get("Apple") - 1) * 1.5 + 2;

                } else totalPrice += map.get("Apple") * 1.5;

            } else totalPrice += map.get(element) * fruitMap.get(element);
        }

        return totalPrice;

    }



}
