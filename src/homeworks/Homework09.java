package homeworks;

import java.util.Arrays;
import java.util.Collections;

public class Homework09 {
    public static void main(String[] args) {

        System.out.println("\n------------ TASK1 ------------\n");

        int[] numbers = {-4, 0, 1, -7, 0, 5, -7, 45, 45};

        boolean isFound = false;
        int firstDuplicate = 0;

        for (int i = 0; i < numbers.length; i++) {
            for (int j = i + 1; j < numbers.length; j++) {

                if (numbers[i] == numbers[j] && !isFound) {
                    firstDuplicate = numbers[i];
                    isFound = true;
                    break;
                }
            }
        }
        System.out.println(isFound ? firstDuplicate : "There is no duplicates");

        System.out.println("\n------------ TASK2 ------------\n");

        String[] words = {"Z", "abc", "z", "123", "#"};

        boolean isFound1 = false;
        String firstDup = "";

        for (int i = 0; i < words.length; i++) {
            for (int j = i + 1; j < words.length; j++) {

                if (words[i].toLowerCase().equals(words[j].toLowerCase()) && !isFound1) {
                    firstDup = words[i];
                    isFound1 = true;
                    break;
                }
            }
        }
        System.out.println(isFound1 ? firstDup : "There is no duplicates");

        System.out.println("\n------------ TASK3 ------------\n");

        int[] numberList = {0, -4, -7, 0, 5, 10, 45, -7, 0};
        String duplicate = "";

        if (numberList.length < 1) System.out.println("There is no duplicates");
        for (int i = 0; i < numberList.length; i++) {
            for (int j = i + 1; j < numberList.length; j++) {

                if (duplicate.contains(numberList[i] + ""))
                    break;

                if (numberList[i] == numberList[j])
                    duplicate += numberList[i];
            }
        }
        System.out.println(duplicate);

        System.out.println("\n------------ TASK4 ------------\n");

        String[] wordList = {"A", "foo", "12", "Foo", "bar", "a", "a", "java"};

        String dup = "";

        if (wordList.length < 1) System.out.println("There is no duplicates");

        for (int i = 0; i < wordList.length - 1; i++) {
            for (int j = i + 1; j < wordList.length; j++) {

                if (wordList[i].equals(wordList[j].toLowerCase()) && !dup.contains(wordList[i]))
                    dup += wordList[i] + "\n";
            }
        }
        System.out.println(dup);

        System.out.println("\n------------ TASK5 ------------\n");

        String[] words5 = {"abc", "foo", "bar"}; // It's a dynamic code. Whatever elements given in an array will be reversed.

        Collections.reverse(Arrays.asList(words5));
        System.out.println(Arrays.toString(words5));

        System.out.println("\n------------ TASK6 ------------\n");
        String str = "Java is fun ";


        String[] words6 = str.split(" ");
        String reverseString = "";

        for(int i = 0; i < words6.length; i++) {
            String word = words6[i];
            String reverseWord = "";

            for(int j = word.length() - 1; j >= 0; j--) {
                reverseWord += word.charAt(j);
            }

            reverseString += reverseWord + " ";
        }

        System.out.println(reverseString);
    }
}
