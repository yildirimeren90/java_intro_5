package homeworks;

import java.sql.Array;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Homework10 {
    public static int countWords(String str) {
        String[] words = str.trim().split("\\s+");
        return words.length;
    }

    public static int countA(String str2) {
        int count = 0;
        Pattern pattern = Pattern.compile("[aA]");

        for (char c : str2.toCharArray()) {
            Matcher matcher = pattern.matcher(String.valueOf(c));
            if (matcher.matches()) count++;
        }
        return count;
    }
    public static int countPos(List<Integer> str) {

        return (int) str.stream().filter(x -> x > 0).count();
    }


    public static ArrayList<Integer> removeDuplicateNumbers(ArrayList<Integer> numbers) {
        return new ArrayList<>(new HashSet<>(numbers));
    }

    public static ArrayList<String> removeDuplicateElements(ArrayList<String> elements) {
        return new ArrayList<>(new HashSet<>(elements));
    }

    public static String removeExtraSpaces(String str6) {
        return str6.trim().replaceAll("\\s+", " ");
    }
    public static int[] add(int[] a, int[] b) {

        int[] c = new int[(int) Math.max(a.length, b.length)];
        for (int i = 0; i < c.length; i++) {
            if (a.length > i) {
                c[i] += a[i];
            }
            if (b.length > i) {
                c[i] += b[i];
            }
        }
        return c;
    }
    public static int findClosestTo10(int[] numbers) {

        if(numbers.length < 0) System.out.println("There is not enough numbers in the array");

        int closest = 10;

        for (int i = 0; i < numbers.length; i++) {
            if(closest == 10) closest = numbers[i];

            else if(numbers[i] > 0 && numbers[i] < Math.abs(closest))
                closest = numbers[i];

        }
        return closest;
    }

        //Testing method
        public static void main (String[] args){
            System.out.println("\n-----------------TASK1-----------------\n");

            System.out.println(countWords("      Java is fun       ")); // 3


            System.out.println("\n-----------------TASK2-----------------\n");

            System.out.println(countA("TechGlobal is a QA bootcamp"));


            System.out.println("\n-----------------TASK3-----------------\n");

            System.out.println(countPos(new ArrayList(Arrays.asList(-45, 0, 0, 34, 5, 67))));



            System.out.println("\n-----------------TASK4-----------------\n");

            System.out.println(removeDuplicateNumbers(new ArrayList(Arrays.asList(10, 20, 35, 20, 35, 60, 70, 60))));


            System.out.println("\n-----------------TASK5-----------------\n");

            System.out.println(removeDuplicateElements(new ArrayList(Arrays.asList("java", "C#", "ruby", "JAVA", "ruby", "C#", "C++"))));

            System.out.println(removeDuplicateElements(new ArrayList(Arrays.asList("abc", "xyz", "123", "ab", "abc", "ABC"))));

            System.out.println("\n-----------------TASK6-----------------\n");

            System.out.println(removeExtraSpaces("   I   am      learning     Java      "));

            System.out.println(removeExtraSpaces("Java  is fun    "));

            System.out.println("\n-----------------TASK7-----------------\n");

            int[] arr1 = {3, 0, 0, 7, 5, 10};
            int[] arr2 = {6, 3, 2};

            System.out.println(Arrays.toString(add(arr1, arr2)));


            System.out.println("\n-----------------TASK8-----------------\n");

            int[] num = {10, -13, 5, 70, 15, 57};
            System.out.println((findClosestTo10(num)));
            int[] num2 = {10,-13,8,12,15,-20};
            System.out.println(findClosestTo10(num2));

    }
    }




