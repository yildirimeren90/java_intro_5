package collections;

import java.util.Arrays;
import java.util.Set;

public class Practice05_Maps {
    public static void main(String[] args) {

        String[] objects = {"Remote", "Mouse", "Mouse", "Keyboard", "iPad"};


        boolean hasMouse = false;

        for(String object : objects){
            if(object.equals("Mouse")){
                hasMouse = true;
                break;
            }
        }

        System.out.println(hasMouse);


    }
}