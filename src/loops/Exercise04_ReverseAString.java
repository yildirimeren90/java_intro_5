package loops;

import utilities.ScannerHelper;

public class Exercise04_ReverseAString {
    public static void main(String[] args) {

        String name = ScannerHelper.getAName();

        for (int i = 0; i <= name.length()-1; i++) {
            System.out.println();
        }
    }
}
