package loops.fori_loops;

import utilities.ScannerHelper;

public class Exercise01_PrintNumbersAscending {
    public static void main(String[] args) {
        for (int i = 100; i >= 0; i--) {
            System.out.println(i);
        }
        for (int j = 0; j <= 10; j++){
            if(j % 2 == 0) System.out.println(j);
        }
        for (int j = 1; j <= 50; j++) {
            if (j % 5 == 0) System.out.println(j);
        }
        int num = ScannerHelper.getANumber();
            for (int g = 0; g <= num;  g++) {
                 if(g % 2 == 1) System.out.println(g);
            }
    }
}
