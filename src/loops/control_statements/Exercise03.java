package loops.control_statements;

public class Exercise03 {
    public static void main(String[] args) {

        for (int i = 1; i <= 3; i++) { // outer loop
            System.out.println(i);
            for (int j = 1; j <= 5; j++) { // inner loop
                System.out.println("\t" + j);
            }
        }
        /*
Write a program to print below rectangle
* * * * * *
*         *
*         *
*         *
*         *
*         *
*         *
* * * * * *


 */
        for (int i = 1; i <= 8; i++) {
           if(i == 1 || i == 8) System.out.println("********");
           else System.out.println("*      *");
        }
        for (int i = 1; i <= 50; i++) {
            for (int j = 1; j < i; j++) {
                System.out.print("A");
            }
            System.out.println();
        }




    }
}