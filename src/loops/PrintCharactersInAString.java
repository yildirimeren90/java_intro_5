package loops;

public class PrintCharactersInAString {
    public static void main(String[] args) {
        String str = "TechGlobal";
        for (int i = 0; i <= str.length()-1; i++) {
            System.out.println(str.charAt(i));
        }
    }

}
