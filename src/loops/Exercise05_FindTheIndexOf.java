package loops;

public class Exercise05_FindTheIndexOf {
    public static void main(String[] args) {

    /*
    Create a public static method named as findLastIndexOf() and it will take a String,
    and a char then it will return the index of the last occurrence of the char. If the
    char doesn't exist in the String then return -1.
    Example:
    "I love Java", 'a' ->  10
    "Banana", 'a' -> 5
    "Banana", 't' -> -1
 */

    }
}
