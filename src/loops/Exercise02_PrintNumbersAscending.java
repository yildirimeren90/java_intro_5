package loops;

public class Exercise02_PrintNumbersAscending {
    public static void main(String[] args){

        int start = 1;
        while (start <= 10) {
            start += 1;
            start++;
        }

        }

}
