package arraylist_linkedlist_vector;

import java.util.ArrayList;

public class Exercise01_countElements {

    public static void main(String[] args) {

        ArrayList<String> colors = new ArrayList<>();
        colors.add("Blue");
        colors.add("Brown");
        colors.add("Pink");
        colors.add("Yellow");
        colors.add("Red");
        colors.add("Purple");
        System.out.println(colors);
        System.out.println(colors.size());
        int counter = 0;
        for (String color : colors) {
            if (color.length() == 6) {
                counter++;
            }
            System.out.println(color);}
        System.out.println(counter);
        System.out.println(colors.size());

        int counterO = 0;
        for (String color : colors) {
            if (color.contains("o")) {
                counterO++;
            }
        }
        System.out.println(counterO);

        int count = 0;
        for (int i = 0; i < colors.size(); i++) {

        if (colors.get(i).toLowerCase().contains("o")) {
                count++;
            }
        }
        System.out.println(count);
    }
}
