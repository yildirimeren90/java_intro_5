package arraylist_linkedlist_vector;

import java.util.LinkedList;

public class _06_Sorting {
    public static void main(String[] args) {

        LinkedList<Integer> numbers = new LinkedList<>();
        numbers.add(10);
        numbers.add(-3);
        numbers.add(5);
        numbers.add(15);
        System.out.println(numbers);

        LinkedList<String> words = new LinkedList<>();
        words.add("Remote");
        words.add("Phone");
        words.add("Laptop");
        System.out.println(words);


    }
}
