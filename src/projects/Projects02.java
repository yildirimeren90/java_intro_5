package projects;

import java.util.Scanner;

public class Projects02 {
    public static void main(String[] args) {
        Scanner inputReader = new Scanner(System.in);

        /*----------------------------TASK1----------------------------*/
        int num1;
        int num2;
        int num3;
        System.out.println("Please enter 3 numbers");
        num1 = inputReader.nextInt();
        num2 = inputReader.nextInt();
        num3 = inputReader.nextInt();
        System.out.println("The product of the numbers entered is = " + (num1) * (num2) * (num3));


        /*----------------------------TASK2----------------------------*/

        String firstName;
        String lastName;
        int dateOfBirth;
        System.out.println("What is your first name?");
        firstName = inputReader.nextLine();
        inputReader.nextLine();
        System.out.println("What is your last name?");
        lastName = inputReader.nextLine();
        inputReader.nextLine();
        System.out.println("What is your year of birth?");
        dateOfBirth = inputReader.nextInt();
        System.out.println(firstName + " " + lastName + "'s " + "age is = " + Math.abs(dateOfBirth - 2022));

        /*----------------------------TASK3----------------------------*/

        String fullName;
        int weight = inputReader.nextInt();
        System.out.println("What is your full name?");
        fullName = inputReader.nextLine();
        System.out.println("What is your full weight?");
        weight = inputReader.nextInt();
        inputReader.nextLine();

        System.out.println(fullName + "'s weight is = " + (weight * 2.205));

        /*----------------------------TASK4----------------------------*/
        String student1Fname;
        String student2Fname;
        String student3Fname;

        int student1Age;
        int student2Age;
        int student3Age;

        System.out.println("What is your full name?");
        student1Fname = inputReader.nextLine();
        System.out.println("What is your age?");
        student1Age = inputReader.nextInt();
        inputReader.nextLine();
        System.out.println("What is your full name?");
        student2Fname = inputReader.nextLine();
        System.out.println("What is your age?");
        student2Age = inputReader.nextInt();
        inputReader.nextLine();
        System.out.println("What is your full name?");
        student3Fname = inputReader.nextLine();
        System.out.println("What is your age?");
        student3Age = inputReader.nextInt();
        inputReader.nextLine();
        System.out.println(student1Fname + "'s age is " + student1Age);
        System.out.println(student2Fname + "'s age is " + student2Age);
        System.out.println(student3Fname + "'s age is " + student3Age);

        System.out.println("The average age is = " + (student1Age + student2Age + student3Age) / 3);
        System.out.println("The eldest age is = " + Math.max(Math.max(student1Age, student2Age), student3Age));
        System.out.println("The youngest age is = " + Math.min(Math.min(student1Age, student2Age), student3Age));


    }
}
