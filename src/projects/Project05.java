package projects;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.util.Arrays;
import java.util.List;

public class Project05 {
    public static void main(String[] args) {
        System.out.println("--------------  TASK 1  -------------");

        int[] number1 = {10, 7, 7, 10, -3, 10, -3};
        findGreatestAndSmallestWithSort(number1);

        System.out.println("---------------- TASK 2 --------------");
        int[] number2 = {10, 7, 7, 10, -3, 10, -3};
        finDGreatestAndSmallest(number2);

        System.out.println("--------------  TASK 3  -------------");
        int[] number3 = {10, 5, 6, 7, 8, 5, 15, 15};
        findSecondGreatestAndSmallestWithSort(number3);

        System.out.println("--------------  TASK 4 -------------");
        int[] number4 = {10, 5, 6, 7, 8, 5, 15, 15};
        findSecondSmallestGreatest(number4);

        System.out.println("-------------- TASK 5 --------------");

        String[] str = {"foo", "bar", "Foo", "bar", "6", "abc", "6", "xyz"};
        duplicatedElements(str);

        System.out.println("------------- TASK 6 -----------------");

        String[] elements = {"pen","eraser","pencil","pen","123","abc","pen","eraser"};
        List<String> list = Arrays.asList(elements);
        System.out.println(mostRepeatedElement(list));

    }



    public static void findGreatestAndSmallestWithSort(int[] numbers) {
        Arrays.sort(numbers);
        System.out.println(Arrays.toString(numbers));
        if (numbers.length > 0) {
            System.out.println("Smallest = " + numbers[0]);
            System.out.println("Greatest = " + numbers[numbers.length - 1]);
        } else {
            System.out.println("Array is empty");
        }
    }

    public static void finDGreatestAndSmallest(int[] numbers) {
        int maxNumber = Integer.MIN_VALUE;
        int minNumber = Integer.MAX_VALUE;

        for (int number : numbers) {
            if (number > maxNumber)
                maxNumber = number;

            if (number < minNumber) {
                minNumber = number;

            }

        }
        System.out.println("Smallest= " + minNumber);
        System.out.println("Greatest = " + maxNumber);

    }

    public static void findSecondGreatestAndSmallestWithSort(int[] number) {

        int secondGreatest = 0, secondSmallest = 0;
        Arrays.sort(number);

        for (int i = number.length - 2; i >= 0; i--) {

            if (number[i] != number[number.length - 1]) {
                secondGreatest = number[i];
                break;
            }
        }
        System.out.println("Second Greatest = " + secondGreatest);

        for (int i = 0; i < number.length; i++) {
            secondSmallest = number[i + 2];
            break;
        }
        System.out.println("Second Smallest = " + secondSmallest);
    }
    public static int findGreatest(int[] numbers) {

        int max = Integer.MIN_VALUE;

        for (int num : numbers) {
            if (num > max)
                max = num;
        }
        return max;
    }

    public static int findSmallest(int[] numbers) {
        int min = Integer.MAX_VALUE;

        for (int num : numbers) {
            if (num < min)
                min = num;
        }
        return min;
    }

    public static void findSecondSmallestGreatest(int[] numbers) {
        int secondGreatest = Integer.MIN_VALUE;
        int secondSmallest = Integer.MAX_VALUE;

        for (int num : numbers) {
            if (num > secondGreatest && num < findGreatest(numbers))
                secondGreatest = num;

            if (num < secondSmallest && num > findSmallest(numbers))
                secondSmallest = num;
        }

        System.out.println("Second smallest = " + secondSmallest);
        System.out.println("Second greatest = " + secondGreatest);
    }

    public static void duplicatedElements(String[] str) {

        String duplicate = "";

        for (int i = 0; i < str.length - 1; i++) {
            for (int j = i + 1; j < str.length; j++) {
                if (duplicate.contains(str[i] + "")) {
                    break;
                }

                if (str[i] == str[j]) {
                    duplicate += str[i] + ",";
                }
            }
        }
        System.out.println(Arrays.toString(duplicate.split(",")));
    }

    public static String mostRepeatedElement(List<String> names) {

        String repeatedName = "";
        int count = 0;
        int maxCount = 0;

        for (String name : names) {
            count = 1;
            for (String innerName : names) {
                if (name.equals(innerName))
                    count++;

                if (count > maxCount) {
                    maxCount = count;
                    repeatedName = name;
                }
            }
        }
        return repeatedName;
    }
}