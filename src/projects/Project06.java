package projects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class Project06 {

    public static int countMultipleWords(String[] words) {

        int count = 0;
        for(String word : words) {
            word = word.trim();
            if(Pattern.matches("[\\w ]+ [\\w ]+", word))
                count++;
        }
        return count;
    }

    public static int[] removeNegativeNumbers(int[] num) {
        List<Integer> list = new ArrayList<>();
        for (int n : num) {
            if (n >= 0) {
                list.add(n);
            }
        }
        int[] result = new int[list.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = list.get(i).intValue();
        }
        return result;
    }


    public static boolean validatePassword(String password) {
        for (int i = 0; i < password.length(); i++) {
            if (password.length() < 16 && password.length() <= 8) {
            char ch = password.charAt(i);
            if (ch >= 'A' && ch <= 'Z' || ch >= 'a' && ch <= 'z' || 33 >= '0' && ch <= 64) {
                return true;
            }}}
        return false;
    }

    public static boolean validateEmailAddress(String email) {

        return Pattern.matches("[\\w]{2,}@[\\w]{2,}\\.[\\w]{2,}",email);
    }











    public static void main(String[] args) {

        System.out.println("\n**********************TASK1**********************\n");

        String[] word = {"foo", "", " ", "foo bar", "java is fun", " ruby "};
        System.out.println(countMultipleWords(word));
        System.out.println("\n**********************TASK2**********************\n");

        int[] array = {2, -5, 6, 7, -10, -78, 0, 15};
        System.out.println(Arrays.toString(removeNegativeNumbers(array)));

        System.out.println("\n**********************TASK3**********************\n");
    String pass = "Abcd123!";
        System.out.println(validatePassword(pass));

        System.out.println("\n**********************TASK4**********************\n");
        String mail = "abcd@gmail.com";
        System.out.println(validateEmailAddress(mail));


    }
}