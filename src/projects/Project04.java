package projects;




import java.util.Random;
import java.util.Scanner;

public class Project04 {
    public static void main(String[] args) {

        System.out.println("\n=====================TASK1=====================\n");

        String str;
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter a string:");
        str = input.nextLine();

        if (str.length() >= 8) {
            if (str.length() % 2 == 0) {
                System.out.println(str.substring(str.length() - 4) + (str.substring(str.length() / 2 - 1, str.length() / 2 + 1)) + (str.substring(0, 4)));
            } else {
                System.out.println(str.substring(str.length() - 4) + str.charAt(str.length() / 2) + (str.substring(0, 4)));
            }
        } else {
            System.out.println("This String does not have 8 characters");
        }

        System.out.println("\n=====================TASK2=====================\n");

        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter a string:");
        String s = input.nextLine();


        System.out.println("\n=====================TASK3=====================\n");
        String str1 = "These books are so stupid";
        String str2 = "I like idiot behaviors";
        String str3 = "I had some stupid t-shirts in the past and also some idiot look shoes";

        str1 = str1.replace("stupid", "nice");
        str2 = str2.replace("idiot", "nice");
        str3 = str3.replace("stupid", "nice");
        str3 = str3.replace("idiot", "nice");
        System.out.println(str1);
        System.out.println(str2);
        System.out.println(str3);

        System.out.println("\n=====================TASK4=====================\n");

        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter your name:");
        String name = input.nextLine();
        if (name.length() > 2) {
            if (name.length() % 2 == 0) {
                System.out.println(name.substring(name.length() / 2 - 1, name.length() / 2 + 1));
            } else {
                System.out.println((name.charAt(name.length() / 2)));
            }
        } else {
            System.out.println("Invalid input!!!");
        }

        System.out.println("\n=====================TASK5=====================\n");

        Scanner scann = new Scanner(System.in);
        System.out.println("Please enter a county:");
        String country = input.nextLine();
        if (country.length() < 5) {
            System.out.println("Invalid input");
        } else {
            System.out.println(country.substring(2, country.length() - 2));
        }

        System.out.println("\n=====================TASK6=====================\n");

        Scanner sca = new Scanner(System.in);
        System.out.println("Please enter your address:");
        String address = input.nextLine();
        System.out.println(address.replace('a', '*').replace('e', '#')
                .replace('i', '+').replace('u', '$').
                replace('o', '@').replace('A', '*').replace('E', '#')
                .replace('I', '+').replace('U', '$').
                replace('O', '@'));

        System.out.println("\n=====================TASK7=====================\n");
        Random random = new Random();
        int randomNumber1 = random.nextInt(25 ) + 1;
        int randomNumber2 = random.nextInt(25 ) + 1;


        System.out.println("\n=====================TASK8=====================\n");
        Scanner scanner1 = new Scanner(System.in);
        System.out.println("Please enter a sentence:");
        String sentence = input.nextLine();

        int count = 0;

        for (char c : sentence.toCharArray()) {
            if (c == ' ') count++;
        }
        System.out.println(count + 1);

        System.out.println("\n=====================TASK9=====================\n");
        Scanner scanner2 = new Scanner(System.in);
        System.out.println("Please enter a positive number");
        int number = input.nextInt();
        for (int i = 1; i <= number; i++) {
            if (i % 2 == 0) System.out.println("Foo");
            else if (i % 3 == 0) System.out.println("Barr");
            else if (i % 6 == 0) System.out.println("FooBarr");
            else System.out.println(i);
        }
        System.out.println("\n=====================TASK10=====================\n");

        System.out.println("Please enter a word");
        String word = "", reverseWord = "";
        word = scanner.nextLine();

        int strLength = word.length();

        for (int i = (strLength - 1); i >= 0; --i) {
            reverseWord = reverseWord + word.charAt(i);
        }

        if (word.toLowerCase().equals(reverseWord.toLowerCase())) {
            System.out.println(word + " is a Palindrome String.");
        } else {
            System.out.println(word + " is not a Palindrome String.");
        }

        System.out.println("\n=====================TASK11=====================\n");
        System.out.println("Please enter a sentence");
        String st = "";
        st = scanner.nextLine();

        int counter1 = 0;
        int counter2 = 0;

        for (int i = 0; i < st.length(); i++) {
            if (st.length() < 1) {
                System.out.println("This sentence does not have any characters");
            } else if (st.charAt(i) == 'a') {
                counter1++;
            }else if (st.charAt(i) == 'A'){
                counter2++;
        }} System.out.println("This sentence has " + (counter1 + counter2) + " a or A letters");


    }
}