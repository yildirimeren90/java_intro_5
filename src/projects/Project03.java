package projects;

public class Project03 {
    public static void main(String[] args) {

        System.out.println("\n============ TASK 1 ==============\n");
        String s1 = "24", s2 = "5";

        int i1 = Integer.parseInt(s1), i2 = Integer.parseInt(s2);

        System.out.println("The sum of 24 and 5 = " + (i1 + i2));
        System.out.println("The subtraction of 24 and 5 = " + (i1 - i2));
        System.out.println("The division of 24 and 5 = " + ((double) i1 / (double) i2));
        System.out.println("The multiplication of 24 and 5 = " + (i1 * i2));
        System.out.println("The remainder of 24 and 5 = " + (i1 % i2));

        System.out.println("\n============ TASK 2 ==============\n");
        int rNumber = (int) (Math.random() * 35 - 1 + 1) + 1;
        System.out.println(rNumber);

        if (rNumber == 1 || rNumber % 2 == 0 || rNumber % 3 == 0
                || rNumber != 2 && rNumber != 3) {
            System.out.println("The number is not a prime number");
        } else {
            System.out.println("The number is a prime number");
        }


        System.out.println("\n============ TASK 3 ==============\n");
        int rNum1 = (int) (Math.random() * 50 - 1 + 1) + 1;
        int rNum2 = (int) (Math.random() * 50 - 1 + 1) + 1;
        int rNum3 = (int) (Math.random() * 50 - 1 + 1) + 1;

        System.out.println(rNum1);
        System.out.println(rNum2);
        System.out.println(rNum3);

        System.out.println("Lowest number is = " + (Math.min(Math.min(rNum2, rNum3), rNum1)));
        System.out.println("Greatest number is = " + (Math.max(Math.max(rNum2, rNum3), rNum1)));

        if (rNum1 > rNum2 && rNum1 > rNum3 && rNum2 < rNum3) {
            System.out.println("Middle number is = " + rNum3);
        } else if (rNum2 > rNum1 && rNum2 > rNum3 && rNum3 < rNum1) {
            System.out.println("Middle number is = " + rNum1);
        } else if (rNum3 > rNum2 && rNum3 > rNum1 && rNum1 < rNum2) ;
        {
            System.out.println("Middle number is = " + rNum2);
        }

        System.out.println("\n============ TASK 4 ==============\n");

        char myChar = 'd';
        int num1 = myChar;

        if (num1 >= 65 && num1 <= 90 || num1 >= 97 && num1 <= 122) {
            System.out.println("The character is letter");
            if (num1 >= 65 && num1 <= 90) {
                System.out.println("The character is upper case letter");
            } else {
                System.out.println("The character is lowercase");
            }
        } else {
            System.out.println("Invalid character detected");
        }

        System.out.println("\n============ TASK 5 ==============\n");

        char char1 = 'e';
        int n1 = char1;

        System.out.println("My character is a letter = " + char1);
        if (n1 >= 97 && n1 <= 122 || n1 >= 65 && n1 <= 90) {
            System.out.println("The character is = " + n1 + " is a letter.");

            if (n1 == 97 || n1 == 101 || n1 == 105 || n1 == 111 || n1 == 117 ||
                    n1 == 65 || n1 == 69 || n1 == 73 || n1 == 79 || n1 == 85) {
                System.out.println("My character is = " + n1+ " is vowel");
            } else if (n1 >= 97 && n1 <= 122 || n1 >= 65 && n1 <= 90) {
                System.out.println("My character is = " + n1 + " is = consonant");
            }
        } else {
            System.out.println("Invalid character detected");
        }

        System.out.println("\n============ TASK 6 ==============\n");

        char myChar2 = '5';
        int ascii = myChar2;

        if (myChar2 >= 32 && myChar2 <= 47 || myChar2 >= 58 && myChar2 <= 64 ||
                myChar2 >= 92 && myChar2 <= 96) {
            System.out.println("Special character is = " + myChar2);
        } else {
            System.out.println("Invalid character detected");

            System.out.println("\n============ TASK 7 ==============\n");

            char myChar3 = 23;
            int ascii1 = myChar3;
            if (ascii1 >= 65 && ascii1 <= 90 || ascii1 >= 97 && ascii1 <= 122) {
                System.out.println("Character is a letter");
            } else if (ascii1 >= 48 && ascii1 <= 57) {
                System.out.println("Character is a digit");
            } else {
                System.out.println("Character is a special character");
            }


        }
    }

}