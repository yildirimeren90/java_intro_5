package projects;

import java.util.Arrays;

public class Project08 {

    public static int findClosestDistance(int[] array) {
Arrays.sort(array);
int dMin = array[array.length - 1];
       if(array.length > 2) {
           for (int i = 0; i < array.length - 1; i++) {
               dMin = Math.min(dMin, Math.abs(array[i] - array[i + 1]));
           }
           return dMin;
       }
return -1;
    }


    public static int findSingleNumber(int[] array) {


        for (int i = 0; i < array.length; i++) {
            int count = 0;
            for (int j = 0; j < array.length; j++) {
                if (array[i] == array[j]) {
                    count++;
                }
            }
            if (count == 1) {
                return array[i];
            }
        }
        return -1;
    }

    public static char findFirstUniqueCharacter(String s) {

        for (int i = 0; i < s.length(); i++) {
            boolean found = true;
            for (int j = 0; j < s.length(); j++) {
                if (i != j && s.charAt(i) == s.charAt(j)) {
                    found = false;
                    break;
                }
            }
            if (found) {
                return s.charAt(i);
            }
        }
        return ' ';
    }

    public static int findMissingNumber(int[] array) {

        int counter = array[0];

        for (int i = 0; i < array.length; i++) {
            if (array[i] == counter) {
                counter++;
            }
        }
        return counter;
    }



    public static void main(String[] args) {
        System.out.println("\n---------------TASK1---------------\n");
        int[] arr1 = {4, 8, 7, 16};
        System.out.println(findClosestDistance(arr1));
        int[] arr2 = {10, -5, 20, 50, 100};
        System.out.println(findClosestDistance(arr2));

        System.out.println("\n---------------TASK2---------------\n");

        int[] arr4 = {5, 5, 3, -1, 3, 7, -1};
        System.out.println(findSingleNumber(arr4));

        int[] arr5 = {6, 9, 9, 3, 3, 7, 7, 10};
        System.out.println(findSingleNumber(arr5));

        int[] arr6 = {3, 3, 8, 8, 5, 9, 9};
        System.out.println(findSingleNumber(arr6));

        System.out.println("\n---------------TASK3---------------\n");

        System.out.println(findFirstUniqueCharacter("Hello"));
        System.out.println(findFirstUniqueCharacter("abc abc d"));
        System.out.println(findFirstUniqueCharacter("abab"));

        System.out.println("\n---------------TASK4---------------\n");

        int[] arr7 = {2, 4};
        System.out.println(findMissingNumber(arr7));

        int[] arr8 = {2, 3, 1, 5};
        System.out.println(findMissingNumber(arr8));

        int[] arr9 = {4, 7, 8, 6};
        System.out.println(findMissingNumber(arr9));

        int[] arr10 = {15, 17, 18, 19, 20};
        System.out.println(findMissingNumber(arr10));

        int[] arr11 = {22, 23, 24, 26, 27};
        System.out.println(findMissingNumber(arr11));

    }

    }

